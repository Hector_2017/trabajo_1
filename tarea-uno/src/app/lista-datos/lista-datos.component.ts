import { Component, OnInit } from '@angular/core';
import { DatosUsuario } from '../models/datos-usuario.model';

@Component({
  selector: 'app-lista-datos',
  templateUrl: './lista-datos.component.html',
  styleUrls: ['./lista-datos.component.css']
})
export class ListaDatosComponent implements OnInit {

  datos: DatosUsuario[];

  constructor() { 
    this.datos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre: string, email:string): boolean {
    //agregamos elementos al arrays datos
    this.datos.push(new DatosUsuario(nombre, email));
    console.log(this.datos);
    return false; //para no cargar la pagina de nuevo 
  }

  elegido(d: DatosUsuario) {
    this.datos.forEach(function(x) { x.setSelected(false);} );
    d.setSelected(true);
  }
}
