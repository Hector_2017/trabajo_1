import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SaludadorComponent } from './saludador/saludador.component';
import { ListaDatosComponent } from './lista-datos/lista-datos.component';

@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent,
    ListaDatosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
