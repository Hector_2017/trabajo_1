export class DatosUsuario {

    private selected: boolean = false;

    constructor(public name:string, public email:string) {
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(selected: boolean){
        this.selected = selected;
    }
}