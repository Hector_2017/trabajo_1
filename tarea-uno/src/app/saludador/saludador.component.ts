import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { DatosUsuario } from '../models/datos-usuario.model';

@Component({
  selector: 'app-saludador',
  templateUrl: './saludador.component.html',
  styleUrls: ['./saludador.component.css']
})
export class SaludadorComponent implements OnInit {

  @Input()dato!:DatosUsuario;

  @HostBinding('attr.class') cssclass = 'col-md-4';
 @Output() clicked: EventEmitter<DatosUsuario>;
  constructor() { 
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ver() {
    this.clicked.emit(this.dato);
  }
}
